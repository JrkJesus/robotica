# MEMORIA ROBÓTICA

### CLASE 1 - 5/10/2017
Introducción a MATLAB. Operaciones con matrices y dibujar un cubo.


### CLASE 2 - 10/10/2017
Continuamos con la introducción a MATLAB:

* Animar el cubo para que gire
* Inicio del movimiendo de la cabeza del robot


### CLASE 3 - 11/10/2017
Comenzamos con la práctica 1. Primeras aproximaciones al movimiento de los robots NXT.


### CLASE 4 - 17/10/2017
Continuamos con el movimiento de la cabeza del robot. Estos movimientos deben seguir una función suave (seno) para no desgastar en exceso las piezas.

Debemos dibujar un punto que se mueva, en tiempo real, por la función definida.


### CLASE 5 - 18/10/2017
Seguimos con la práctica 1:

1. ¿Funciona control_motor_2? Lo usamos para comprobar la conexión robot-PC

2. Diapositivas actuadores:
    * Los motores eléctricos DC son los más usuales
        - Con escobillas <-- Los Lego NXT usan de este tipo
        - Paso a paso <-- No necesitan sensor que mida la posición (los anteriores sí)
    * Accionadores hidráulicos y neumáticos. Para movimientos de mucha fuerza
        - Los hidráulicos para movimientos precisos y lentos
        - Los neumáticos para movimientos que no requieren precisión

3. Sensores. En los Lego, es importante conocer el ángulo de giro del motor:
    * Encoders. Hay de dos tipos:
        - Absolutos. El sensor indica el eje en cualquier instante
        - Relativos. Indican cuántos pulsos se han contado y hacia qué lado
    * Los Lego NXT usan encoders relativos

4. Con MATLAB, es posible acceder a estos registros (encoders) mediante dos funciones:
    * NXT_ResetMotorPosition() - resetea el encoder
    * NXT_GetOutputState() - obtiene los valores almacenados en el encoder
    * .RotationCount - obtiene un histórico de los valores extraídos con la función anterior

5. Experimento encoders de los motores A y B (ruedas)
    * Reseteamos posición del motor
    * Leemos el encoder (el resultado debe ser 0) - con NXT_GetOutputState y .RotationCount
    * Movemos ruedas a mano
    * Leemos de nuevo (el resultado ya no debe ser 0)

6. Experimento lectura_giro_cabeza
    * Debemos hacer un programa con un bucle temporal que lea el encoder de la cabeza en tiempo real, durante 10 segundos
    * El objetivo es dibujar una gráfica con el movimiento
    * Más detalles en el archivo lectura_giro_cabeza
    * IMPORTANTE: Limpiar los arrays al inicio del experimento para evitar errores


### CLASE 6 - 19/10/2017
Seguimos con la práctica 1:

1. Comprobamos el funcionamiento de los robots ejecutando el archivo lectura_giro_cabeza

2. Vamos a limitar la potencia del motor entre -100 y +100 (regulador) - control_potencia
    * La potencia se calcula en función del error
    * potencia(i) = K · error(i)
    * potencia[i] = int8(...)
    * Probar para k = 0.1, 0.25, 0.4 y 0.9
    * Para 0.1 se mueve muy lento, para 0.25 el movimiento es estable, para 0.4 oscila ligeramente y en 0.9 se vuelve completamente oscilante

3. Probar con k > 1..5..10
    * Es recomendable guardar fotos de las gráficas para comentar los resultados
    * La conclusión es que un controlador por sí solo no es estable, es importante configurarlo con los parámetros correctos

4. Ahora la referencia no será 90, sino que utilizaremos la función referencia_cabeza
    * referencia[i] = referencia_cabeza(A, P, D, t)
    * En un bucle de 20 segundos, delay de 2 segundos y periodo de 4 segundos

5. Habrá que incorporar mediciones del sonar


### CLASE 7 - 24/10/2017
Finalizamos la práctica 1:

1. Confirmamos que control_potencia se mueva correctamente

2. Añadimos al bucle de control_potencia, medición del sonar
    * Con k = 0.4, la cabeza oscila. Con k = 0.25 mantiene una pequeña oscilación
    * Colocamos k = 0.15 para un movimiento adecuado

3. Colocamos al robot frente a la pared, con la esquina mas cercana a la derecha

4. Dibujamos el entorno medido por el sonar

5. Hacemos foto del robot (para la memoria)

6. Queda así finalizada la práctica


### CLASE 8 - 25/10/2017
Comenzamos la práctica 2:

1. Creación del README.md de la práctica 2 con todos los datos sobre la máquina de estados a diseñar para el robot
    * Estados
    * Transiciones
    * Leyendas
    * Scripts
    * Funciones
    * Ver el archivo maquina_estado_inicial para el diagrama de estados inicial

2. Leemos el código que se nos entrega, para comprobar que está correcto y que entendemos que realiza
    * Es importante confirmar que las variables de los motores coinciden con las nuestras
    * Cargamos el programa en el robot y comprobamos que funciona correctamente:
        - El robot se mueve en línea recta hacia alante
        - Si detecta un obstáculo (distancia de sonar = 35), se para y anda marcha atrás
        - Realiza esto en bucle

3. Entonces debemos comenzar a implementar el giro de la cabeza en el estado 3
    * Importante: hay dos switch, el primero para el cambio de estado y el segundo para la acción a ejecutar en cada estado

4. Finalizamos la clase sin acabar el giro de la cabeza


### CLASE 9 - 26/10/2017
Continuamos la práctica 2:

1. Debemos finalizar la implementación del estado 3 (giro_cabeza):
```Matlab
    while
    % switch transiciones
        case estado_activo 3
            transicion;
            [tiempo(i)-tiempo(transicion)] > incremento de tiempo;

    % switch salidas
        case estado_activo 3
            función del giro_cabeza;
    end while
```

2. Ajustamos los tiempos del incremento del tiempo (7 segundos lo óptimo en nuestro caso)

3. Ploteamos gráfico que muestra el movimiento de la cabeza y la distancia medida por el sonar

4. Vemos la clasificación de los distintos sensores


### CLASE 10 - 31/10/2017
Lo siguiente a programar es el controlador de orientación. Hoy vamos a dar los primeros pasos para su diseño

1. Vamos a partir de la máquina de estados básica

2. En lugar de andar hacia atrás, el robot deberá girar y escoger el camino que menos obstaculos tenga y avanzar hacia alante en ese sentido
```Matlab
    % transiciones
    case estado = 4
        if incrementoT > 2 (esta condición es transitoria)
            estado = 3;
            transicion = i;
        end

    % salidas
    case estado = 4
        Power1 = 50;
        Power2 = -Power1 (para que gire sobre sí mismo);
        Traction_motor_control;
```

3. Comenzamos con las pruebas
    * Para hacer el controlador, es mejor que trabajemos en grados en lugar de radianes. Una vez funcione correctamente, podemos pasarlo a radianes

4. Se nos va a entregar un fichero que se encarga de calcular la odometría del robot, de momento no va a ser necesario editarlo
    * calculo_odometria(...)
    * yaw es el ángulo en grados
    * theta el ángulo en radianes

5. Con este fichero vamos a poder plotear el recorrido completo seguido por el robot

### CLASE 11 - 02/11/2017
Continuamos la práctica 2:

1. Para que la odometría se calcule de forma correcta, debemos ajustar l y r:
    * l es la distancia desde el centro del robot a las ruedas - 6 en este caso
    * r es el radio de las ruedas - 3 en este caso

2. Ajustamos el giro del robot para que gire 136 grados (con el delay de la transición)

3. A tener en cuenta:
    * Las ruedas deslizan ligeramente
    * El comando de movimiento no se envia a ambas ruedas a la vez
    * La precisión en el parámetro l es vital

4. En este momento el giro del robot varía porque se hace en función al tiempo

5. Vamos a hacer que el giro del robot se realice en función de una referencia:
```Matlab
    referencia_yaw = 90
    error_yaw(i) = referencia_yaw(i) - yaw(i)
    Power1 = int8(K_giro * error_yaw(i)
    Power2 = -Power1
```

* referencia_yaw en el futuro se obtendrá en función de la posición del robot
* El tiempo de transición se hace en función al error_yaw

6. Implementamos lo anterior:
    * El robot gira 90 grados la primera vez, después solamente mueve la cabeza
    * Esto se debe a la referencia, que está fija en 90 grados
    * La solución: referencia_yaw = 90 + yaw(transicion)

7. Aplicada la solución el robot gira correctamente

8. Por último, ajustamos el movimiento de la cabeza porque no lo completaba correctamente

9. Finaliza la clase.El próximo día se nos entregará un script que se encarga de detectar en qué dirección en la que hay menos obstáculos


### CLASE 12 - 07/11/2017
Estudiamos un fichero que se encarga de calcular la referencia en función de la posición actual y vemos teoría:

1. Se ha subido un fichero y una imagen al aula virtual:
    * Fichero medidas sonar
    * Circuito de prueba

2. Vamos a ver el fichero medidas sonar (Calcula el ángulo)
    * calculo_referencia
    * Es necesario revisar el giro de los motores, ya que no todos los robots tienen los motores asignados de igual manera - giro_cabeza(i) > 0...
    * La referencia se va a calcular en función del ángulo de giro y la distancia

3. Estudiada la función calculo_referencia, comenzamos con la unidad 4 (Robots Móviles)
    * 4.1. Introducción: Preliminares y conceptos
        - Motivación: ¿Qué vamos a hacer?
        - Preliminares y conceptos
        - Tipos de navegación

4. Fatality a la asignatura. Jesus 1 optimiza calculo_referencia en 5 minutos


### CLASE 13 - 08/11/2017
Continuamos la práctica 2:

1. Incorporamos calculo_referencia al movimiento

2. Probamos el robot con dos cajas y se mueve correctamente en función de dónde encuentra menos obstáculos (media aritmética)

3. Probamos el robot en el circuito y lo recorre perfectamente


### CLASE 14 - 09/11/2017
Continuamos la práctica 2:

1. Vamos a dibujar al robot siguiendo su recorrido

2. Para ello, debemos tener en cuenta el sistema de referencia:
    * Vamos a tener una matriz que empleamos para transformar nuestro sistema local (el robot) al sistema global

3. Siguiendo el método de trabajo usado para el cubo que animamos en la Introducción a MATLAB, dibujamos el robot
```Matlab
    function pinta_robot(posicion)
      x = posicion(1);                        % Xl = x(end)
      y = posicion(2);                        % Yl = y(end)
      theta = posicion(3); - en radianes      % thetal = theta(end)
      z = 0; - dibujamos en 2D                % z = 0
```

4. Dibujamos al robot siguiendo el recorrido

5. Ahora vamos a añadir la cabeza a nuestro dibujo, que tiene su propio sistema de referencia local respecto a la cabeza
    * Por lo tanto, debemos hacer Tr*Tc*Px
        - Donde Px son los puntos de la cabeza en el sistema local
        - Tr la matriz de transformación del robot al sistema global
        - Tc la matriz de transformación de la cabeza al sistema de la cabeza (su sistema global)

6. Dibujamos el robot con las ruedas (que pueden dibujarse en función de su propio sistema local de referencia) y la cabeza moviéndose


### CLASE 15 - 14/11/2017
Continuamos la práctica 2:

1. Debemos implementar completamente la función pinta_robot

2. Como la tenemos implementada y funcionando, ajustamos el dibujo:
    * La Y queda desviada: ajustamos el radio de la rueda y lo ponemos en 2.75

3. Reducimos el incremento del periodo del movimiento de la cabeza
    * Delay_delantero reducido a 0.5s
    * Delay_trasero reducido a 1.2s
    * Periodo reducido a 4s
    * Total = 5.7 segundos

4. El próximo día vamos a dibujar también los obstáculos, utilizando el sonar
    * Tr*Tc*[d 0 0 1]'
    * A partir de cierta distancia(cuando se pierde el sónar), no pintamos el punto
    * Pasamos los parámetros alpha (giro_cabeza en radianes) y distancia


### CLASE 16 - 15/11/2017
Continuamos con la práctica 2:

1. Recorremos el circuito y dibujamos el recorrido en tiempo real

2. Confirmamos que se dibujan los muros correctamente

3. Vemos teoría sobre los grados de libertad (D.O.F)
    * Restricciones de movimiento
    * Vo es la velocidad lineal, Wo la velocidad angular
    * V = W · R, V = W/curvatura
    * En nuestro robot, la Vy siempre va a ser 0 debido a las limitaciones físicas del mismo

4. Modelo directo e inverso
    * Modelo directo para simulación y la odometría
    * Modelo indirecto para el control del robot
        - derivada de alpha en función del tiempox = fx(V, curvatura) - donde x es cada rueda

### CLASE 17 - 16/11/2017
