%% Control del motor de la cabeza
%     Script que controla el giro de la cabeza
%     y toma medidas para ver hacia donde moverse

%% Varianles:
%     - i:                  indice goblal
%     - transicion:         indice inicio del estado 3
%     - t:                  vector tiempo
%     - k:                  ganancia
%     - giro_cabeza
%     - referencia_cabeza
%     - distancia
%     - error_sonar
%     - Power

                referencia_cabeza(i) = JJ_referencia_cabeza(90, 4, t(transicion)+2, t(i));
                display(referencia_cabeza(i))
                error_sonar(i) = referencia_cabeza(i) - giro_cabeza(i);
                Power = int8(k * error_sonar(i));


% Head_motor_control;


