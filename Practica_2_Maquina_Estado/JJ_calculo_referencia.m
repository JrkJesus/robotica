function [ referencia ] = JJ_calculo_referencia( distancia, giro )
% Distancia y giro deben ir desde transicion hasta el final
% dist_mio = distancia(transicion:end);
% giro_mio = giro_cabeza(transicion:end);

    derecha = [distancia(giro>0)' giro(giro>0)'];
    izquierda = [distancia(giro<=0)' giro(giro<=0)'];

    mean_drch = mean(derecha(:,1))
    mean_izq = mean(izquierda(:,1))

    if( mean_drch >= mean_izq )
        referencia = sum(derecha(:,1) .* derecha(:,2)) / sum(derecha(:,1))
    else
        referencia = sum(izquierda(:,1) .* izquierda(:,2)) / sum(izquierda(:,1))
    end

end

