# PRACTICA 2: MAQUINAS DE ESTADOS
En esta practica configuraremos una maquina de estado tipo maily para definir el comportamiento del robot


## Estados
	- Marcha adelante (Estado inicial)
	- Parada
	- Giro cabeza
	- Giro Robot
	- Marcha atras

## Transiciones
	- Marcha adelante --- (d < dm) --> Parada
	- Parada --- (d > dm) ---> Marcha adelante
	        |--- (d < dm) ---> Giro cabeza
    - Giro Cabeza ---  ---> Giro Robot
    - Giro Robot --- ---> Marcha atras
    - Marcha atras --- (t > tm) ---> Marcha adelante

### Leyendas
	- d: Distancia
	- dm: Distancia minima
	- t: Tiempo
	- tm: Tiempo marcha atras

## Scripts
	- inicio_ladrillo: Script necesario para inicializar la comunicacion con el robot
	- maquina_estado_basica: Script principal con el automata de maily necesario para controlar al robot
	- Signal_read_odo: Toma distancia de sonar y gira la cabeza o los pies.
	- giro_cabeza: Gira la cabeza poco a poco
	- Para_cierra: Apaga todos los motores y sensores
	- Head_motor_control: Manda la señal al motor de la cabeza de moverse

## Funciones
	- JJ_referencia_cabeza: Script que te crea la funcion senosoidal de giro
	