%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% controla el movimiento del robot mediante una m�qunia de estado.
% Utiliza los script: 
% Traction_motro_control.m; Signal_reading_odo.m;
% Para_Cierra.m.
% 29/11/2015. FGB.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clc
close all
clear t
%variables control movimiento cabeza
clear giro_cabeza
clear referencia_cabeza
clear distancia;
clear error_sonar;
clear muro;
clear grid;

%variables control angulo del robot
clear ang_vel;
clear yaw
clear error_yaw
clear referencia_yaw
clear x y theta
clear giro_derecho giro_izquierdo

global radio_rueda
global l % mitad distancia entre ruedas

l=5.95;
radio_rueda=2.75;

% Declaraci�n de sensores
OpenSwitch(SENSOR_2); % definici�n del sensor de contacto detceta colii�n
OpenSwitch(SENSOR_1); % definici�n del sensor de contacto para start
OpenUltrasonic(SENSOR_4); %definici�n del sonar

% Declaraci�n de los motores
motor_derecho = NXTMotor('A'); %Motor_derecho
motor_izquierdo = NXTMotor('B'); %Motor izquierdo
motor_cabeza = NXTMotor('C'); %motor del sonar


%reset del encoder de motores
NXT_ResetMotorPosition(0, false);
NXT_ResetMotorPosition(1, false);
NXT_ResetMotorPosition(2, false);

%indice inicial
i=1;

%referencia tiempo inicial
tstart = tic;

%posici�n inicial cabeza
cabeza=NXT_GetOutputState(2);
giro_cabeza(i)=cabeza.RotationCount;

%valores iniciales de los encoders
giro_derecho(i)=0;
giro_izquierdo(i)=0;
x(i) = 0;
y(i) = 0;
theta(i) = 0;
yaw(i) = 0;
error_yaw(i) = 5;
referencia_yaw(i) = 90;
axis_robot = [-50, 170, -130,130];
axis_x = abs(axis_robot(1)) +  abs(axis_robot(2));
axis_y = abs(axis_robot(3)) +  abs(axis_robot(4));
muro = [];
grid = uint8(ones(axis_y,axis_x)*255);
decrease = 50;

%medida incial
distancia(i) = GetUltrasonic(SENSOR_4);

%--------------------
% Valores iniciales
%--------------------
%tiempo inicial
t(i)=0;
k = 0.15;
k_giro = 0.3;

estado=1; %estado inicial


stop_distance=35; %distancia de para ante obst�culo
t_marcha_atras=1.5; %tiempo de marcha hacia atr�s.

periodo = 4;
delay_delantero = 0.5;
delay_trasero = 1.2;
delta_t = delay_delantero + periodo + delay_trasero;

transicion=1% inicializa la variable que marca el inicio el mov de la cabeza


%comienza el bucle
disp('pulsa el bumper para comenzar')

while(GetSwitch(SENSOR_2)==0)
end

while(GetSwitch(SENSOR_2)==1)
end

disp('comienza el bucle')

while  (GetSwitch(SENSOR_2)==0)
  
    i=i+1; %indice global
    t(i)= toc(tstart); %tiempo global del bucle
    
    
    %---------------------
    %lectura se�ales y calculo del heading
    %-------------------------------
    
    Signal_reading_odo;

    [x(i), y(i), theta(i)] = calculo_odometria(giro_derecho, giro_izquierdo, x, y, theta, i);
    yaw(i) = theta(i)*180/pi;
    estado %muestra el estado del sistema
   
%     muro = pinta_robot([x; y; theta], axis_robot, giro_cabeza, distancia, muro);
%     imshow(grid)
%     hold on
%     muro = [muro; pinta_robot([x; y; theta], giro_cabeza, distancia)];
    new_point =  pinta_robot([x; y; theta], giro_cabeza, distancia);
    
    if( length(new_point) > 0 )
        x_ = floor(new_point(1))+abs(axis_robot(1));
        y_ = floor(new_point(2))+abs(axis_robot(3));
        grid(y_,x_) = grid(y_, x_) - decrease;
    end
%     if( length(muro) > 0 )
%         plot(muro(:,1), muro(:,2), '*k')
%     end
    axis(axis_robot)
    axis equal
    drawnow
    hold off


    %--------------------------------------------------------
    % TRANSICIONES DE ESTADO
    %1-> marchando para adelante
    %2-> parando
    %3-> girando cabeza con sonar
    %4-> girando sobre si mismo
    %5-> Marcha atr�s

        switch estado
            
            case 1 %andando hacia delante
                if (GetUltrasonic(SENSOR_4)<stop_distance) %si la distancia es menor que 35 para
                    estado=2; %transici�n de estado de paro
                    transicion=i; %indice que marca el inicio del estado 2
                end
                
            
            case 2 %parando
                if (vel==0)
                    if distancia(i)>stop_distance
                        estado=1; %la transici�n a estado marcha hacia delante
                        transicion=i; %indice que marca el inicio del estado 1
                    else       
                        estado=3; %transici�n a estado girando cabeza
                        transicion=i; %indice que marca el inicio del estado 3
%                         Crear variable interna                       
                    end
                end
             
            case 3 %girando cabeza    
%                 estado=4; %la transici�n a estado girando robot
                %indice que marca el inicio del estado 4
                if t(i) > t(transicion)+delta_t
                   referencia_yaw(i) = JJ_calculo_referencia(distancia(transicion:end), giro_cabeza(transicion:end)) + yaw(transicion);
%                     referencia_yaw(i) = calculo_r
                   transicion=i;
                    estado = 4
                    % Parar la cabeza! 
                    Power = 0;
                    Head_motor_control;
                end
            case 4 %girando robot
                referencia_yaw(i) = referencia_yaw(i-1);
                if (abs(error_yaw(i-1)) < 2)
                    estado=2; %la transici�n a estado marcha atr�s
                    transicion=i; %indice que marca el inicio del estado 5
                end
           case 5 %marcha atr�s
                if (t(i)-t(transicion)>t_marcha_atras)                  
                    estado=2; %transici�n a estado girando cabeza
                    transicion=i; %indice que marca el inicio del estado 2                 
                end
            
        end %del siwtch
    
   %-----------------------
          
    
    %-------------------------------------------------
    %SALIDAS ASOCIADAS A LOS ESTADOS
    
        switch estado
        
            case 1 %andando hacia delante
            %establece los valores de control 
                vel=20;
                Power1=vel;
                Power2=vel;
                
              %---------------------
              %Manda los comandos de control a los motores
              %-------------
                Traction_motor_control;

            case 2 %parando
              %establece los valores de control 
                vel=0;
                Power1=vel;
                Power2=vel;
                
              %---------------------
              %Manda los comandos de control a los motores
              %-------------
               Traction_motor_control;
        
            case 3 %girando cabeza
                referencia_cabeza(i) = JJ_referencia_cabeza(90, periodo, t(transicion)+delay_delantero, t(i));
                error_sonar(i) = referencia_cabeza(i) - giro_cabeza(i);
                Power = int8(k * error_sonar(i));

                Head_motor_control;
                
            case 4 %girando sobre si mismo
%                 referencia_yaw(i) = 90 + yaw(transicion);
                display(referencia_yaw(i))
                error_yaw(i) = referencia_yaw(i) - yaw(i);
                Power1 = int8(k_giro * error_yaw(i));
                Power2 = -Power1;
                Traction_motor_control;
            
            case 5 %andando hacia atr�s
                %establece los valores de control 
               vel=-20;
               Power1=vel;
               Power2=vel;
                
              %---------------------
              %Manda los comandos de control a los motores
              %-------------
               Traction_motor_control;      
          
        end %del siwtch
    
end %del while

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Para motores y cierra sensores
%-------------------------------

Para_Cierra;
display('Cerrado')

dist_norm = distancia/max(distancia(:));
giro_norm = giro_cabeza/max(giro_cabeza(:));

figure
hold off
title('Sensor sonar')
plot(t,dist_norm, 'b')
hold on
plot(t, giro_norm, 'r')
axis([-1, max(t(:))+1, -2, 2])

