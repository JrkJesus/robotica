function punto_muro =  pinta_robot( posicion, giro_cabeza, distancia )
    % pose = [ x_g, y_g, theta] 
    % theta va en radianes
    % z_g = 0
    punto_muro = [];
    angle = posicion(3,end);
    alpha = giro_cabeza(end)*pi/180;
    d_muro = distancia(end);
    max_d = 150;
    min_d = 6;

    r1 = [8, -6, 0, 1]';
    r11 = [3,-6,0,1]';
    r12 = [3, -9, 0, 1]';
    r13 = [-3, -9, 0, 1]';
    r14 = [-3, -6, 0, 1]';
    r2 = [-9, -6, 0, 1]';
    r3 = [-11, -4.5, 0, 1]';
    r4 = [-11, 4.5, 0, 1]';
    r5 = [-9, 6, 0, 1]';
    r51 = [3, 6,0,1]';
    r52 = [3, 9, 0, 1]';
    r53 = [-3, 9, 0, 1]';
    r54 = [-3, 6, 0, 1]';
    r6 = [8, 6, 0, 1]';
    robot = [r1, r14, r13, r12, r11, r2, r3, r4, r5, r51, r52, r53, r54, r6, r1];
    
    c1 = [-2, -1, 0, 1]';
    c2 = [0, -1, 0, 1]';
    c3 = [0, -3, 0, 1]';
    c4 = [3, -3, 0, 1]';
    c5 = [3, 3, 0, 1]';
    c6 = [0, 3, 0, 1]';
    c7 = [0, 1, 0, 1]';
    c8 = [-2, 1, 0, 1]';
    cabeza = [c1, c2, c3, c4, c5, c6, c7, c8];
    d = 5;
    
    Tr_g_l = [cos(angle), -sin(angle), 0, posicion(1,end) ; sin(angle), cos(angle), 0, posicion(2, end); 0, 0, 1, 0 ; 0, 0, 0, 1 ];
    Tc_g_l = [cos(alpha), -sin(alpha), 0, d ; sin(alpha), cos(alpha), 0, 0; 0, 0, 1, 0 ; 0, 0, 0, 1 ];
    
    robot_g = Tr_g_l * robot;
    cabeza_g = Tr_g_l * Tc_g_l * cabeza;
    if (d_muro > min_d & d_muro < max_d)
        muro = Tr_g_l * Tc_g_l * [d_muro; 0; 0; 1];
        punto_muro = muro(1:2)';
    end
    
    cla
    plot(robot_g(1,:), robot_g(2,:),'b')
    hold on
    plot(posicion(1,:),posicion(2,:),'g')
    plot(cabeza_g(1,:), cabeza_g(2,:),'r')
    
end

