addpath('../Guion_Clase/')
load('maq_estado_circuito_con_muro.mat')
m = length(distancia);
axis_robot = [-20+min(x), max(x)+20, -20+min(y), max(y)+20];
% axis_robot = [-50, 170, -130,130];
axis_x = floor(abs(axis_robot(1)) +  abs(axis_robot(2)));
axis_y = floor(abs(axis_robot(3)) +  abs(axis_robot(4)));
grid = uint8(ones(axis_y,axis_x)*255);
decrease = 50;
close all
Tima_G_L =  [cos(0), -sin(0), 0, abs(axis_robot(1)) ; sin(0), cos(0), 0, abs(axis_robot(3)); 0, 0, 1, 0; 0, 0, 0, 1 ];
for i=1:m
%     muro = pinta_robot([x(1:i); y(1:i); theta(1:i)], axis_robot, giro_cabeza(1:i), distancia(1:i), muro);
    new_point =  pinta_robot([x(1:i); y(1:i); theta(1:i)], giro_cabeza(1:i), distancia(1:i));
    
    if( length(new_point) > 0 )
        new_point_L = [new_point, 0, 1]';
        new_point_G = Tima_G_L * new_point_L;
        x_ = floor(new_point_G(1))+1;
        y_ = floor(new_point_G(2))+1;
        grid(y_,x_) = grid(y_, x_) - decrease;
    end
%     if( length(muro) > 0 )
%         plot(muro(:,1), muro(:,2), '*k')
%     end
    axis(axis_robot)
    axis equal
    drawnow
    hold off
    pause(0.1)
end