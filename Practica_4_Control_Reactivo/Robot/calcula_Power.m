function [ Power1, Power2 ] = calcula_Power( v0, Pini, Pfin, theta )
    global l
    global radio_rueda
% theta = 0;
% v0 = 50;
% r = 30;
% l = 0.5;
% radio_rueda = 1;
% 
    Lh = sqrt( (Pini(1)-Pfin(1))^2 + (Pini(2)-Pfin(2))^2 );
    Delta = (Pini(1)-Pfin(1))*sin(theta) - (Pini(2)-Pfin(2)) * cos(theta);
    p = 2*Delta/Lh^2;
    w = v0 * p;
    Power1 = int8((v0 + w*l)/radio_rueda*1.7854);
    Power2 = int8((v0 - w*l)/radio_rueda*1.7854);
end
