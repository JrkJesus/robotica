function [velocidad, curvatura]=reactivo_sonar(distancia)

    %Definicion de los parametros para la velocidad
    velmax=30;
    velmin=15;
    d_disparo=60;
    d_sat=30;

    %Definicion de la funcion para la velcocidad
    if distancia<d_sat
        velocidad=velmin;
    elseif distancia<d_disparo
        velocidad= velmin+((velmax-velmin)/(d_disparo-d_sat))*(distancia-d_sat);
    else
        velocidad=velmax;
    end

    %Definicion de los parametros para la curvatura
    rhomax=0.40;
    rhomin=0;
    d_disparo=60;
    d_sat=30;

    %Definicion de la funcion
    if distancia<d_sat
        curvatura=rhomax;
    elseif distancia<d_disparo
        curvatura= rhomax-((rhomax-rhomin)/(d_disparo-d_sat))*(distancia-d_sat);
    else
        curvatura=rhomin;
end