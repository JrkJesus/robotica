global l
global radio_rueda
l=5.3;
radio_rueda=2.6;
clc
clear t
%variables control movimiento cabeza
clear giro_cabeza
clear referencia_cabeza
clear distancia;
clear error_sonar;
clear muro;
clear grid;

%variables control angulo del robot
clear ang_vel;
clear yaw
clear error_yaw
clear referencia_yaw
clear x y theta
clear giro_derecho giro_izquierdo

i = 1;
k = 0;
estado = 2;
incr = 17;

v0 = 30;
camino = zeros(121, 2);
camino(:,1) = 0:120;
camino(:,2) = 0:120;

pose0 = [camino(1,:)'; 0];
posef = [camino(end,:)'; 0];
dd = 5;
da = 5;
pdx = pose0(1) + dd*cos(pose0(3));
pdy = pose0(2) + dd*sin(pose0(3));
pax = posef(1) - da*cos(posef(3));
pay = posef(2) - da*sin(posef(3));

xc = [ pose0(1), pdx, camino(10:end-10,1)', pax, posef(1)];
yc = [ pose0(2), pdy, camino(10:end-10,2)', pay, posef(2)];
ds=1; %distancia entre puntos en cm.
camino=funcion_spline_cubica_varios_puntos(xc,yc,ds)';


[ length_camino, column] = size(camino);

plot(camino(:,1), camino(:,2),'*g')
x(i) = pose0(1);
y(i) = pose0(2);
theta(i) = pose0(3);
yaw(i) = 0;
giro_derecho(i)=0;
giro_izquierdo(i)=0;
giro_cabeza(i) = 0;
distancia(i) = 0;

% Declaración de sensores
OpenSwitch(SENSOR_2); % definición del sensor de contacto detecta colisión
OpenSwitch(SENSOR_1); % definición del sensor de contacto para start
OpenUltrasonic(SENSOR_4); %definición del sonar

% Declaración de los motores
motor_derecho = NXTMotor('A'); %Motor_derecho
motor_izquierdo = NXTMotor('B'); %Motor izquierdo
motor_cabeza = NXTMotor('C'); %motor del sonar
NXT_ResetMotorPosition(0, false);
NXT_ResetMotorPosition(1, false);
NXT_ResetMotorPosition(2, false);

disp('pulsa el bumper para comenzar')

while(GetSwitch(SENSOR_2)==0)
end

while(GetSwitch(SENSOR_2)==1)
end

disp('comienza el bucle')

while  (GetSwitch(SENSOR_2)==0)

    % CALCULA POWER DE RUEDAS

%     velocidad = calcula_velocidad_suavizada(camino, [x(i), y(i)])
    indx = minima_distancia(camino,[x(i), y(i)]);
    if(indx+incr > length_camino)
     indx_array = length_camino;
    else
     indx_array = indx+incr;
    end
    punto = camino(indx_array, :);
%     [Power1, Power2] = calcula_Power(velocidad, [x(i), y(i)], punto, theta(i));
%     err = sqrt((x(i)-camino(end,1))^2 + (y(i)-camino(end,2))^2);
    
    [v, cur] = reactivo_sonar(distancia(i));
    [Power1, Power2] = calcula_Power(v, [x(i), y(i)], punto, cur);


    % ENVIO DE POWER
    Traction_motor_control;
    % CALCULO ODOMOETRIA
    i = i+1;
    Signal_reading_odo;

    [x(i), y(i), theta(i)] = calculo_odometria(giro_derecho, giro_izquierdo, x, y, theta, i);
    yaw(i) = theta(i)*180/pi;

    cla
    plot(x,y)
    hold on
    plot(camino(:,1), camino(:,2), 'g')
    drawnow

end
Para_Cierra;
display('Fin')
