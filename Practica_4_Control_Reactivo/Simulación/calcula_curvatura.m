function phi = calcula_curvatura(Pini, Pfin, theta)

%     l = 1.5;
    global l
    Lh = sqrt( (Pini(1)-Pfin(1))^2 + (Pini(2)-Pfin(2))^2 );
    Delta = (Pini(1)-Pfin(1))*sin(theta) - (Pini(2)-Pfin(2)) * cos(theta);
    
    p = 2*Delta/Lh^2;
    
    phi = atan(l*p);
    
end