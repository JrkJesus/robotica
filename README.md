    # ROBOTICA
Practicas de la asignatura de Robotica de la UHU ETSI. En ellas iremos biendo los distintos aspectos de un robot e implementaremos un controlador y alguna IA.

## BLOQUES
 - __Introduccion a matlab__ breves ejercicios para coger la soltura necesaria de Matlab para lo que vamos a usar en robotica
 - __Sistema de control__ controlador simple del robot

## PASOS PARA PONER A PUNTO EL ROBOT (C3PO, R8):
 - En matlab, añadir al path la ruta a la carpeta RWTHMindstormsNXT
 - Instalar Bluesoleil
 - Ejecutar Bluesoleil y buscar a nuestro robot. Una vez encontramos a nuestro robot (00:16:53:09:5A:A1) lo emparejamos
 	- Si no sale para emparejar, dadle a actualizar servicios
 - Click derecho en el puerto y dale a conectar. Importante saber el puerto donde se conecta (COM4) 