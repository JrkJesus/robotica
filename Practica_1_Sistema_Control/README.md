# PRACTICA 1 SISTEMA DE CONTROL
Código del sistema de control del robot



## Apuntes de teoria
* Sistema de referencia global (SRG): Sistema de coordenadas donde tengo los objetos
* Sistema de referencia local (SRL): Sistema de referencia en el objato. No cambia si cambia el objeto
* Transforacion de puntos del SRG al SRL: Pg = T * Pl, donde T es la transformada (4x4) creada con los movimientos que puede tener
Ejemplo: 
```Matlab
T_g_l = [cos(pi/4) -sin(pi/4) 0 5 ; sin(pi/4) cos(pi/4) 0 5; 0 0 1 5 ; 0 0 0 1 ]
cubo_g = T_g_l * cubo_l
```

