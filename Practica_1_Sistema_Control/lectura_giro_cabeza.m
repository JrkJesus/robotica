%% Ejercicio en clase del giro de la cabeza
% Leemos el encoder del motor C para ver la rotaci�n que se est� realizando

clear motor_C giro_C t referencia error

motor_C = NXTMotor('C');
NXT_ResetMotorPosition(2, false);

tstart = tic; %arranca el crono
t(i) = 0;
giro_C(i) = 0;
referencia(i) = 0;
error(i) = 0;

i = 1;
while t(i) < 10
    i = i + 1;
    
    C = NXT_GetOutputState(2);
    giro_C(i) = C.RotationCount;
    
    referencia(i) = 90;
    error(i) = referencia(i) - giro_C(i);
    
    t(i)= toc(tstart);
end

grid on
plot(t, giro_C, 'r');
hold on
plot(t, error, 'g')
plot(t, referencia, 'b');
