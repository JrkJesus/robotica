%% Control del motor de la cabeza

clear motor_C giro_C t referencia error potencia
close all

motor_C = NXTMotor('C');
NXT_ResetMotorPosition(2, false);
OpenUltrasonic(SENSOR_4);

i = 1;
t(i) = 0;
giro_C(i) = 0;
referencia(i) = referencia_cabeza(-90, 4, 2, t(i));
% referencia(i) = -90
error(i) = 0;
potencia(i) = 0;
distancia = GetUltrasonic(SENSOR_4);
k = 0.15;

tstart = tic; % Arranca el crono
while t(i) < 10
    
    i = i + 1;
    t(i)= toc(tstart);
    
    C = NXT_GetOutputState(2);
    giro_C(i) = C.RotationCount;
        
    referencia(i) = referencia_cabeza(-90, 4, 2, t(i));
%     referencia(i) = -90;
    error(i) = referencia(i) - giro_C(i);
    distancia(i) = GetUltrasonic(SENSOR_4);
    potencia(i) = k * error(i);
    
    plot(t, distancia, 'r')
    axis([-2, 12, -2, 257])
    drawnow
    
    if potencia(i) > 100
        potencia(i) = 100;
    elseif potencia(i) < -100
        potencia(i) = -100;
    end
    
    motor_C.Power = int8(potencia(i));
    motor_C.SendToNXT();
end

motor_C.Power = 0;
motor_C.SendToNXT();
motor_C.Stop('off')
grid on
plot(t, giro_C, 'r')
hold on
plot(t, error, 'g')
plot(t, referencia, 'b')
hold off
figure, 
hold on
plot(t, distancia, 'r')
% plot(t, referencia, 'b')

display('Fin');