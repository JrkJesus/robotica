%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Ejemplo de uso de comandos para leer de los encoder 
% de los motores del NXT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear t;
i=1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%definición de los objetos motor
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
motor_A = NXTMotor('A');
motor_B = NXTMotor('B');


%reset del encoder del motor
NXT_ResetMotorPosition(0, false);
% NXT_ResetMotorPosition(2, false);
NXT_ResetMotorPosition(1, false);
%Lectura de los encoder despues del reset
A=NXT_GetOutputState(0);
% B=NXT_GetOutputState(2);
B=NXT_GetOutputState(1);

giro_A(i)=A.RotationCount;
giro_B(i)=B.RotationCount;

%Ponemos en marcha los motores
% giro = 2; % 0 - 2 (1 para adelante)
% v = 0.5;
% 
% motor_A.Power = int8((2-giro)*v*100);
% motor_B.Power = int8((giro-2)*v*100);

motor_A.Power = int8(0.25*50);  
motor_B.Power = int8(0.25*50);

motor_A.SendToNXT(); 
motor_B.SendToNXT(); 

pause(3);

%Lectura de los encoder despues del reset
A=NXT_GetOutputState(0);
% B=NXT_GetOutputState(2);
B=NXT_GetOutputState(1);

giro_A(i+1)=A.RotationCount;
giro_B(i+1)=B.RotationCount;

motor_A.Stop('off'); %los para
motor_B.Stop('off');

display('fin')