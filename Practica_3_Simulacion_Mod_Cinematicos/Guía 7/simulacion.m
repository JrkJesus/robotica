clear all
clc
close all

global mapa
global l
global camino
global pose
global punto

j=1;

%cargamos el camino
mapa = imread('pistaNoPunto.bmp');
mapa(1:end,:,:) = mapa(end:-1:1,:,:);

delta = 17;
optimal_path = A_estrella(mapa, delta);

% optimal_path(1:end,:) = optimal_path(end:-1:1,:);

pose0 = [optimal_path(1, :), 0];
posef = [optimal_path(end, :), pi/2];

dd = 5;
da = 5;
pdx = pose0(1) + dd*cos(pose0(3));
pdy = pose0(2) + dd*sin(pose0(3));
pax = posef(1) - da*cos(posef(3));
pay = posef(2) - da*sin(posef(3));

% Marcha atras
pose0(3) = pose0(3) + pi/2;

xc = [ pose0(1), pdx, optimal_path(2:end-1,1)', pax, posef(1) ];
yc = [ pose0(2), pdy, optimal_path(2:end-1,2)', pay, posef(2)];
ds = 1; %distancia entre puntos en cm.

camino = funcion_spline_cubica_varios_puntos(xc,yc,ds)';
% camino = optimal_path;
[ length_camino, column] = size(camino);

l = 3.5; %distancia entre rudas delanteras y traseras, tambien definido en modelo

%tiempo inicial
t0 = 0;

%final de la simulaci�n
tf = 100;
% tf=50;

%paso de integracion
h = 0.1;
%vector tiempo
t = 0:h:tf;
%indice de la matriz
k = 0;
incr = 5;

%inicializaci�n valores iniciales
pose(:,k+1)=pose0;

t(k+1)=t0;

punto = camino(1,:);

% MARCHA ALANTE

err = 100;
while err > 2,
    %actualizaci�n
    k=k+1;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %valores de los par�metros de control
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
 % estas son las variables de control    
 velocidad = calcula_velocidad_suavizada(camino, pose(1:2, end))
 indx = minima_distancia(camino, pose(1:2,end));
 if(indx+incr > length_camino)
     indx_array = length_camino;
 else
     indx_array = indx+incr
 end
 punto = camino(indx_array, :);
 
 volante = calcula_curvatura(pose(1:2,end), punto, pose(3,end));
 err = sqrt((pose(1, end)-camino(end,1))^2 + (pose(2, end)-camino(end,2))^2)
 
 
 
 %ambas se combinan en la variable conducci�n 
 conduccion=[velocidad volante];
 
%  Marcha atras
%  conduccion = [ -velocidad volante+pi ];
 
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
 %para representar el punto onjetivo sobre la trayectoria
 
%  punto=[30 30];

    
%metodo de integraci�n ruge-kuta y representaci�n gr�fica

    pose(:,k+1)=kuta(t(k),pose(:,k),h,conduccion);
    pause(0.1)

end

% MARCHA ATR�S



display('fin')