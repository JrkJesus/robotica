
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Simulación del movimiento de un robot móvil
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all
close all
clc

j=1;

global l
global camino
global pose
global punto
%cargamos el camino

% camino=load('camino.dat');
camino(:,1) = 0:150;
camino(:, 2) = 0:150;
[ length_camino, column] = size(camino);

global l
l=3.5; %distancia entre rudas delanteras y traseras, tambien definido en modelo

%Estos son distintos ejemplos de Condiciones iniciales 

%pose0=[15; 15; -pi/2];

%pose0=[30; 30; 0];

pose0=[0; 0; pi/2];

% Marcha atras
pose0(3) = pose0(3) + pi;

%tiempo inicial
t0=0;

%final de la simulaci�n
tf=100;
% tf=50;

%paso de integracion
h=0.1;
%vector tiempo
t=0:h:tf;
%indice de la matriz
k=0;
incr = 20;

%inicializaci�n valores iniciales
pose(:,k+1)=pose0;

t(k+1)=t0;

punto = camino(100,:);

% while (t0+h*k) < tf,
err = 100;
while err > 2,
    %actualizaci�n
    k=k+1;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %valores de los par�metros de control
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
 % estas son las variables de control    
 velocidad = calcula_velocidad_suavizada(camino, pose(1:2, end))
 indx = minima_distancia(camino, pose(1:2,end));
 if(indx+incr > length_camino)
     indx_array = length_camino;
 else
     indx_array = indx+incr
 end
 punto = camino(indx_array, :);
 
 volante = calcula_curvatura(pose(1:2,end), punto, pose(3,end));
 err = sqrt((pose(1, end)-camino(end,1))^2 + (pose(2, end)-camino(end,2))^2)
 
 
 
 %ambas se combinan en la variable conducci�n 
%  conduccion=[velocidad volante];
 
%  Marcha atras
 conduccion = [ -velocidad, volante+pi ];
 
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
 %para representar el punto onjetivo sobre la trayectoria
 
%  punto=[30 30];

    
%metodo de integraci�n ruge-kuta y representaci�n gr�fica

    pose(:,k+1)=kuta(t(k),pose(:,k),h,conduccion);
    pause(0.1)

end

display('fin')