function [ veloc ] = calcula_velocidad_suavizada( camino, pose )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
% y = (0.35*(x-2))^2
% y representa la velocidad
% x representa la distancia
    fin = camino(end,:)';
    x_veloc = sqrt((pose(1)-fin(1))^2 + (pose(2)-fin(2))^2);
    veloc = (0.40*(x_veloc))^2;
%     veloc = round(x);
    if veloc > 30
        veloc = 30;
    end

end

