global l 
global radio_rueda
l=5.3;
radio_rueda=2.6;
clc
clear t
%variables control movimiento cabeza
clear giro_cabeza
clear referencia_cabeza
clear distancia;
clear error_sonar;
clear muro;
clear grid;

%variables control angulo del robot
clear ang_vel;
clear yaw
clear error_yaw
clear referencia_yaw
clear x y theta
clear giro_derecho giro_izquierdo

i = 1;
estado = 2;

v0 = 30;
pose0 = [0, 0, 0];

camino = 0:120;


r = 40;
x(i) = 0;
y(i) = 0;
theta(i) = 0;
yaw(i) = 0;
giro_derecho(i)=0;
giro_izquierdo(i)=0;
giro_cabeza(i) = 0;
distancia(i) = 0;

% Declaración de sensores
OpenSwitch(SENSOR_2); % definición del sensor de contacto detecta colisión
OpenSwitch(SENSOR_1); % definición del sensor de contacto para start
OpenUltrasonic(SENSOR_4); %definición del sonar

% Declaración de los motores
motor_derecho = NXTMotor('A'); %Motor_derecho
motor_izquierdo = NXTMotor('B'); %Motor izquierdo
motor_cabeza = NXTMotor('C'); %motor del sonar
NXT_ResetMotorPosition(0, false);
NXT_ResetMotorPosition(1, false);
NXT_ResetMotorPosition(2, false);

disp('pulsa el bumper para comenzar')

while(GetSwitch(SENSOR_2)==0)
end

while(GetSwitch(SENSOR_2)==1)
end

disp('comienza el bucle')

while  (GetSwitch(SENSOR_2)==0)
    
    % CALCULA POWER DE RUEDAS
    v0 = calcula_velocidad_suavizada(target, [x(i), y(i)]);
    [Power1, Power2] = calcula_Power(v0, [x(i), y(i)], target, theta(i));
    
    % ENVIO DE POWER
    Traction_motor_control;
    % CALCULO ODOMOETRIA
    i = i+1;
    Signal_reading_odo;
    
    [x(i), y(i), theta(i)] = calculo_odometria(giro_derecho, giro_izquierdo, x, y, theta, i);
    yaw(i) = theta(i)*180/pi;

    plot(x,y)
    drawnow
    
end
Para_Cierra;
display('Fin')