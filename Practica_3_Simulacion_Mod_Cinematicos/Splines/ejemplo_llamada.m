%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Ejemplo de la llamada a la función 
% funcion_spline_cubica_varios_puntos.m
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all
close all
clc
j=1;

global l
global camino
global pose
global punto
% xc=[0 10 40 80 80 80];
% yc=[0 0 40 40 100 120];






%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Simulación del movimiento de un robot móvil
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%cargamos el camino

% camino=load('camino.dat');
% camino(:,1) = 0:150;
% camino(:, 2) = 0:150;

l=3.5; %distancia entre rudas delanteras y traseras, tambien definido en modelo

%Estos son distintos ejemplos de Condiciones iniciales 

%pose0=[15; 15; -pi/2];

%pose0=[30; 30; 0];
camino1 = zeros(81, 2);
camino1(:,1) = 0:80;
camino1(:,2) = 0:80;

pose0=[0, 0,0];



posef = [80, 80, 0];
dd = 10;
da = dd;
pdx = pose0(1) + dd*cos(pose0(3));
pdy = pose0(2) + dd*sin(pose0(3));
pax = posef(1) - da*cos(posef(3));
pay = posef(2) - da*sin(posef(3));

% Marcha atras
% pose0(3) = pose0(3) + pi/2;
ds=3;
xc = [ pose0(1), pdx, camino1(10:end-10,1)', pax, posef(1) ];
yc = [ pose0(2), pdy, camino1(10:end-10,2)', pay, posef(2)];
 %distancia entre puntos en cm.
camino=funcion_spline_cubica_varios_puntos(xc,yc,ds)';
[ length_camino, column] = size(camino);
%tiempo inicial
t0=0;

%final de la simulación
tf=100;
% tf=50;

%paso de integracion
h=0.1;
%vector tiempo
t=0:h:tf;
%indice de la matriz
k=0;
incr = 3;

%inicialización valores iniciales
pose(:,k+1)=pose0;

t(k+1)=t0;


% while (t0+h*k) < tf,
err = 100;
while err > 1.5,
    %actualización
    k=k+1;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %valores de los parámetros de control
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
 % estas son las variables de control    
 velocidad = calcula_velocidad_suavizada(camino, pose(1:2, end))
 indx = minima_distancia(camino, pose(1:2,end));
 if(indx+incr > length_camino)
     indx_array = length_camino;
 else
     indx_array = indx+incr
 end
 punto = camino(indx_array, :);
 
 volante = calcula_curvatura(pose(1:2,end), punto, pose(3,end));
 err = sqrt((pose(1, end)-camino(end,1))^2 + (pose(2, end)-camino(end,2))^2)
 
 
 
 %ambas se combinan en la variable conducción 
 conduccion=[velocidad volante];
 
%  Marcha atras
%  conduccion = [-velocidad, volante];
 
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
 %para representar el punto onjetivo sobre la trayectoria
 
%  punto=[30 30];

    
%metodo de integración ruge-kuta y representación gráfica

    pose(:,k+1)=kuta(t(k),pose(:,k),h,conduccion);
    pause(0.1)
%     waitforbuttonpress;
end

display('fin')



