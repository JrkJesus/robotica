# INTRODUCCION A MATLAB
Codigo de distintos ejercicios de introduccion a matlab para robotica.

## Codigos de utilidad
  - Actualizacion en tiempo real
```Matlab
    start = tic;
    i = 1;
    tiempo(i) = toc(start);
    while tiempo(i) < 5
        cla % limpiar pantalla
        plot(tiempo(i), 0, 'or')
        hold on
        plot([0,5],[0,0])
    %     hold off  % Realmente no necesario
        drawnow   % le obligo a que pinte la grafica
        i = i+1;
        tiempo(i) = toc(start);
    end
```

## Comando de matrices

  - Operacion elemento a elemento en vez de matricial:
```Matlab
    A = ones(2,3);
    B = randn(2,3)*5;
    C = A.*B;
    D = C.^2;
```
    
  - Autovalores y autovectores
```Matlab   
    A = randn(10,10)*23;
    [ M L ] = eig(A);
```