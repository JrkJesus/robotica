function  [ salida ] = referencia_cabeza( amplitud, periodo, delay, tiempo )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

    salida = amplitud * sin((2*pi/periodo)*(tiempo-delay));
    salida(tiempo<delay | tiempo>(delay+periodo) ) = 0;
end