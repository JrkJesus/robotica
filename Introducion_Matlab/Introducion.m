%% Actividad 5. 
A = [ 11, 12, 13, 14; 21, 22, 23, 24; 31, 32, 33, 34; 41, 42, 43, 44];
% apartado A
    Ap = A(2:3, 2:3);
%Apartado B
    R = [A, [ones(2,2); eye(2,2)]];


%% Actividad 6. 
% Apartado A
    t = 0:0.1:30;
    f = -5*exp(-0.2*t).*cos(0.9*t-pi/6)+0.8*exp(-2*t)+5;
    plot(t, f)
    axis([0, 30, 1, 8])
    
% Apartado b
    [fmax, indx] = max(f);
    tmax = t(indx);
    hold on
    plot(tmax,fmax,'or')
    
%% Ejercicio 7
    p1 = [-1, 1, 0, 1]';
    p2 = [-1, -1, 0, 1]';
    p3 = [1, -1, 0, 1]';
    p4 = [1, 1, 0, 1 ]';
    cuadrado = [p1, p2, p3, p4, p1];
    plot(cuadrado(1,:),cuadrado(2,:))
    axis([-2,2,-2,2])


%% Actividad 8.
    p1 = [-1, 1, 0, 1]';
    p2 = [-1, -1, 0, 1]';
    p3 = [1, -1, 0, 1]';
    p4 = [1, 1, 0, 1 ]';
    p11 = [-1, 1, 3, 1]';
    p21 = [-1, -1, 3, 1]';
    p31 = [1, -1, 3, 1]';
    p41 = [1, 1, 3, 1 ]';
    cubo = [p1, p2, p3, p4, p1, p11, p21, p2, p21, p31, p3, p31, p41, p4, p41, p11];
    plot3(cubo(1,:),cubo(2,:), cubo(3,:))
    axis([-5,5,-5,5])
    axis equal
%     axis(x1,x2, y1,y2, z1,z2)

%% Actividad 8.- MODIFICACION
    p1 = [-1, 1, 0, 1]';
    p2 = [-1, -1, 0, 1]';
    p3 = [1, -1, 0, 1]';
    p4 = [1, 1, 0, 1 ]';
    p11 = [-1, 1, 3, 1]';
    p21 = [-1, -1, 1.5, 1]';
    p31 = [1, -1, 1.5, 1]';
    p41 = [1, 1, 3, 1 ]';
    
    angle = pi/6;
    local = [ 5 5 5 ];
    cubo_l = [p1, p2, p3, p4, p1, p11, p21, p2, p21, p31, p3, p31, p41, p4, p41, p11];
    T_g_l = [cos(angle), -sin(angle), 0, local(1) ; sin(angle), cos(angle), 0, local(2); 0, 0, 1, local(3) ; 0, 0, 0, 1 ];
    cubo_g = T_g_l * cubo_l;
    hold on
    plot3(cubo_g(1,:),cubo_g(2,:), cubo_g(3,:))
    axis([-10,10,-10,10, 0, 10])
    axis equal
%     axis(x1,x2, y1,y2, z1,z2)


%% Ejercicio clase
p1 = [-1, 1, 0, 1]';
p2 = [-1, -1, 0, 1]';
p3 = [1, -1, 0, 1]';
p4 = [1, 1, 0, 1 ]';
p11 = [-1, 1, 3, 1]';
p21 = [-1, -1, 1.5, 1]';
p31 = [1, -1, 1.5, 1]';
p41 = [1, 1, 3, 1 ]';

angle = 0;
local = [ 5 5 5 ];
cubo_l = [p1, p2, p3, p4, p1, p11, p21, p2, p21, p31, p3, p31, p41, p4, p41, p11];
T_g_l = [cos(angle), -sin(angle), 0, local(1) ; sin(angle), cos(angle), 0, local(2); 0, 0, 1, local(3) ; 0, 0, 0, 1 ];
cubo_g = T_g_l * cubo_l;

while angle < 2*pi
    cla % limpiar pantalla
    plot3(cubo_g(1,:),cubo_g(2,:), cubo_g(3,:))
    axis([-10,10,-10,10, 0, 10])
%     hold off  % Realmente no necesario
    drawnow   % le obligo a que pinte la grafica
    angle = angle+0.01;
    T_g_l = [cos(angle), -sin(angle), 0, local(1) ; sin(angle), cos(angle), 0, local(2); 0, 0, 1, local(3) ; 0, 0, 0, 1 ];
    cubo_g = T_g_l * cubo_l;
end
display('end')


%% Ejercicio clase 2
close all
clear all
% t = -4*pi:0.01:8*pi;
p = 10;
d = 5;
amplitud = pi/2;
% y = referencia_cabeza(amplitud, p, d, t);
start = tic;
i = 1;
tiempo(i) = toc(start);
while tiempo(i) < d+p+5
    
    plot(tiempo(i), referencia_cabeza(amplitud, p, d, tiempo(i)), 'or')
    hold on
    plot(tiempo, referencia_cabeza(amplitud, p, d, tiempo))
    axis([0, d+p+5, -amplitud, amplitud])
    hold off 
%     hold off  % Realmente no necesario
    drawnow   % le obligo a que pinte la grafica
    i = i+1;
    tiempo(i) = toc(start);
end