function solucion=cinematica_inv_munneca(destino, theta)

% clear
% clc


xf=destino(1);
yf=destino(2);
zf=destino(3);


%theta=1*pi/4;%�ngulo que forma la mu�eca con la horinontal

%theta=0.8;%�ngulo que forma la mu�eca con la horinontal
l=10; % distancia desde el punto central de la mu�eca a la articulaci�n
l=13.0;
beta=atan2(yf,xf);

%Proyecci�n de la mu�eca sobre el plano x,y
punto=desplazamiento(xf,yf,zf)*Rotacionz(beta)*Rotaciony(theta)*[-l;0;0;1];
x0=punto(1);
y0=punto(2);
z0=punto(3);

solucion=[x0 y0 z0];

% plot3([xf x0],[yf y0],[zf z0],'r')
% hold on
% plot3([xf],[yf ],[zf ],'ok')
% plot3([x0],[y0 ],[z0 ],'og')
% grid on

