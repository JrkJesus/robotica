
function mensaje(configuracion, port)

inicio(port);
global control
global s
control=zeros(1,4);

%configuracion=[0 0 0 0 0 0];

%pulsos=angulo2pulso(configuracion);

pulsos=convierte_angulos_pulsos(configuracion);

warning('off')
mandar(pulsos); 
warning('on')
fclose(s);


function mandar(pulsos)
  
global fin
global msg
global control  
global s


velocidad='250';
pulsos=pulsos';
tamano=size(pulsos);
longitud=tamano(2);
for i=1:longitud
    canal0=pulsos(1,i);
    canal1=pulsos(2,i);canal2=pulsos(3,i);canal3=pulsos(4,i);canal4=pulsos(6,i);canal5=pulsos(5,i);
    canal0=num2str(canal0)
    canal1=num2str(canal1) 
    canal2=num2str(canal2)
    canal3=num2str(canal3)
    canal4=num2str(canal4)%esta es la pinza
    canal5=num2str(canal5)
  
    
    mensage=['#' '0' 'P' canal0 'S' velocidad '#' '1' 'P' canal1 'S' velocidad '#' '2' 'P' canal2 'S' velocidad  '#' '3' 'P' canal3 'S' velocidad '#' '4' 'P' canal4 'S' velocidad '#' '5' 'P' canal5 'S' velocidad char(13)]
    
    fprintf(s,mensage)

    msg='X';

       
    
    recibir();

end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function recibir()
  
global fin
global msg
global control
global s

pause(1)
respuesta='+';
msg=['Q' char(13)];
fprintf(s,msg)

 while not(strcmp(respuesta,'.'))
     respuesta = getSentence();
     pause(1)
     fprintf(s,msg)
 end

    

disp('la posici�n articular se ha alcanzado')


msg='On';



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% esta funci�n configura y abre el puerto serie y establece una lectura
% continua asincrona
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function inicio(port)

%clear
clc


global fin
global msg
global control
global port
global s

%port='COM3';
BAUDRATE=115200;
out = instrfind;
if isempty(out)
    disp('Todas las puertas estan disponibles'); 
end
[m,n] = size(out);
for f=1:n
    if strcmp(out(f).Status,'open') & strcmp(out(f).Port,port)
        fclose(out(f));
        disp(['La conexion ', out(f).Port, ' fue cerrada']);
    end
end


%abre el puerto
s = serial(port);

%configura el puerto

s.BaudRate          = BAUDRATE;
s.DataBits          = 8;
s.Parity            = 'none';
s.StopBits          = 1;
s.InputBufferSize   = 1;
s.OutputBufferSize  = 100;
s.Timeout           = 30; 
s.ReadAsyncMode     = 'continuous';
fopen(s);
if strcmp(get(s,'Status'),'closed')
    disp(['Error, imposible establecer conexion con el puerto ', port]);
    closePort(port);
    s = -1;
    return
else
    disp(['Establecida conexion con el puerto ', port]);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function recibido = getSentence();
global s

recibido = [];
n=0;
while isempty(recibido)
    if s.BytesAvailable
       recibido = fscanf(s);
    end

end


