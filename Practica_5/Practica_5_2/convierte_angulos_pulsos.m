function pulsos=convierte_angulos_pulsos(angulos)
% function pulsos=convierte_angulos_pulsos(angulos)
% Convierte angulos en radianes a anchos de pulso.
% Parametros.
% angulos: angulos en radianes.
% Retorno.
% pulsos: anchos de pulso.

if (angulos(1)>pi/2)
    angulos(1)=pi/2;
end
if (angulos(1)<-pi/2)
    angulos(1)=-pi/2;
end

alfa = pi/2 - angulos(1);
razon = alfa / pi;

%pulsos(1) = 600 + razon*(2475-565);
pulsos(1) = 580 + razon*(2475-565);

if (angulos(2)>pi/2)
    angulos(2)=pi/2;
end
if (angulos(2)<-pi/2)
    angulos(2)=-pi/2;
end

alfa = pi/2 - angulos(2);
razon = alfa / pi;
pulsos(2) = 670 + razon*(2240-700);

if (angulos(3)>4*pi/5)
    angulos(3)=4*pi/5;
end
if (angulos(3)<-pi/5)
    angulos(3)=-pi/5;
end

alfa = angulos(3) + pi/5;
razon = alfa / pi;

pulsos(3) = 530 + razon*(2400-600);


if (angulos(4)>pi/2)
    angulos(4)=pi/2;
end
if (angulos(4)<-pi/2)
    angulos(4)=-pi/2;
end


alfa = pi/2 - angulos(4);
razon = alfa / pi;

pulsos(4) = 650 + razon*(2400-600);


if (angulos(5)>pi/2)
    angulos(5)=pi/2;
end
if (angulos(5)<-pi/2)
    angulos(5)=-pi/2;
end


alfa = pi/2 - angulos(5);
razon = alfa / pi;
pulsos(5) = 800 + razon*(2200-800);



if (angulos(6)>0)
    angulos(6)=pi/6;
end
if (angulos(6)<=0)
    angulos(6)=-(0.2+pi/6);
end


alfa = angulos(6);
razon = alfa / (pi/6);
pulsos(6) = 1500 + razon*(1600-1500);
pulsos=fix(pulsos);