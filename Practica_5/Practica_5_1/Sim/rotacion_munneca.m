function angulo=rotacion_munneca(theta,articulares,punto,destino)


q2=articulares(2);
q3=articulares(3)
q1=articulares(1);


%Si el punto donde se ha de situar el extremo del brazo cae en el semiplano
%negativo del plano definido por el manipulador la soluci�n geom�trica
%utilizada hace que el �ngulo q(1) sea el �ngulo original del plano que contiene el punto
%m�s pi y por tanto la expresi�n para q4 se modifica. 

%destino es el dato inicial, el punto donde colocaremos el centro de la
%pinza. 

angulo=atan2(destino(2),destino(1))

%�ngulo es el angulo del plano que contiene el punto y al manipulador

punto2=[punto';1]
%punto2 es el punto inicial de la mu�eca, donde se situar� el brazo pero en
%coordenadas extendidas
Rotacionz(angulo)

%cordenadas del punto inicial de la mu�eca en el plano que contiene el manipulador
punto2=Rotacionz(-angulo)*punto2

    if punto2(1)>=0

        angulo=theta-(q2+q3-(pi/2));

    else
   
    angulo=(pi-theta)-(+q2+q3-(pi/2));
    end
