%metodo de integracion de Euler

function salida=kuta(t,Y,h,modelo,qf)

salida=zeros(size(Y));%la salida debe ser un vector columna

k1=h*feval(modelo,t,Y,qf);
k2=h*feval(modelo,t+h/2,Y+(1*k1/2),qf);
k3=h*feval(modelo,t+h/2,Y+(1*k2/2),qf);
k4=h*feval(modelo,t+h,Y+(1*k3),qf);
salida=Y+(k1+2*k2+2*k3+k4)/6;
