function referencias = ejercicio7

configuracion1 = [12 0 16 pi/2];
articulaciones1 = calculos_articulares(configuracion1);

configuracion2 = [13 0 15 pi/2];
articulaciones2 = calculos_articulares(configuracion2);

configuracion3 = [14 0 14 pi/2];
articulaciones3 = calculos_articulares(configuracion3);

configuracion4 = [15 0 12 pi/2];
articulaciones4 = calculos_articulares(configuracion4);

configuracion5 = [17 0 8 pi/2];
articulaciones5 = calculos_articulares(configuracion5);

configuracion6 = [17 0 12 pi/2];
articulaciones6 = calculos_articulares(configuracion6);

referencias = [articulaciones1 -1; articulaciones2 -1; articulaciones3 -1; articulaciones4 -1; articulaciones5 1; articulaciones6 1];