
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Simulaci�n del movimiento de un manipulador de 3 DOF
% El modelo no es real solo tiene com funci�n permitir la animaci�n
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all
clc

%definici�n del modelo

f='modelo';
j=1;

%Condiciones iniciales 

x0=[0 0 0 0 0 0];

t0=0;

%final de la simulaci�n
tf=20;
%paso de integracion
h=0.1;

%indice de la matriz
k=0;

%trayectoria articular
         

% referencias=[x0;...
%              0    0.6396    0.3214    2.1806         0        -1;...
%              0    0.4402    0.9932    1.7082         0        -1;...
%              0    0.4402    0.9932    1.7082         0         1;...
%              0    0.6396    0.3214    2.1806         0         1];

% Ejercicio 1
% referencias=[x0;...
%              0    0.6396    0.3214    2.1806         0        1;...
%              0    0.4402    0.9932    1.7082         0        1;...
%              0    0.4402    0.9932    1.7082         0       -1;...
%              0    0.6396    0.3214    2.1806         0       -1];

% Ejercicio 2
% referencias=[x0;...
%              0    0.6396    0.3214    2.1806         0        -1;...
%              0    0.4402    0.9932    1.7082         0        -1;...
%              0    0.4402    0.9932    1.7082         0         1;...
%              0    0.6396    0.3214    2.1806         0         1;...
%              0    0.6396    0.3214    2.1806         0        -1;...
%              2    0.6396    0.3214    2.1806         0        -1];
         
% Ejercicio 7
referencias = [x0;...
               ejercicio7];

j=size(referencias); %indice para el tama�o de la matriz de referncias articulares

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%inicializaci�n variables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

x(:,k+1)=x0;

t(k+1)=t0;

fin=0;

estado=1; %estado inicial

i=1; %indice para incrementar la fila de la matriz referencias
qf=referencias(i,:); %referncia inicial

error=qf-x(:,k+1)';

errortotal=sqrt(error*error');

%-----------------------------------
%configuraci�n de la pieza
posicion_pieza=desplazamiento(17,0,0);

angulo_pieza=0;

rot_pieza=Rotacionz(angulo_pieza);

matriz_pieza=posicion_pieza*rot_pieza;

matriz_pieza_mano=zeros(4,4);
%-------------------------------------

[matriz_mano matriz_pieza matriz_pieza_mano]=animacion(x(:,k+1),matriz_pieza,matriz_pieza_mano);



while (not(fin))
  
   
    %actualizaci�n
    k=k+1;
      
    %maquina de estados Transiciones
     
     switch estado
          case 1, 
           if (errortotal<0.01)
               estado=2;
           end
           
         case 2,
            if (i==j(1,1)) 
                estado=3
            else
                estado=1; 
                i=i+1;
            end

    end 
        
%salida de los estados: valores de los par�metros de control


 switch estado
          case 1, 
             qf=referencias(i,:);
          case 3
             fin=1

 end      
    

 
  
 
%metodo de integraci�n ruge-kuta

x(:,k+1)=kuta(t(k),x(:,k),h,f,qf);

t(k+1)=t(k)+h;

error=qf-x(:,k+1)';

errortotal=sqrt(error*error');

[matriz_mano matriz_pieza matriz_pieza_mano]=animacion(x(:,k+1), matriz_pieza, matriz_pieza_mano);


%estado
       
end




