%% Ejercicio 1. Apartado A
angulo = [pi/2 -pi/4 pi/4 pi/2 0];
pos_ini = [0 0 0];
pos_fin = [0 0 0];

dibujo_brazo_3d(angulo, pos_ini, pos_fin);

angulo = [pi/2 pi/4 -pi/4 pi/2 0];
waitforbuttonpress;
cla

dibujo_brazo_3d(angulo, pos_ini, pos_fin);


%% Ejercicio 1. Apartado B
angulo = [0 0 0 0 0];
waitforbuttonpress;
cla

dibujo_brazo_3d(angulo, pos_ini, pos_fin);


angulo = [pi/2 pi/2 pi/2 0 0];
waitforbuttonpress;
cla

dibujo_brazo_3d(angulo, pos_ini, pos_fin);


%% Ejercicio 2. Apartado A

waitforbuttonpress;
articulaciones = cinematica_inv_brazo([10 10 10]);


%% Ejercicio 2. Apartado B

angulo = [articulaciones 0 0];
dibujo_brazo_3d(angulo, pos_ini, pos_fin);


%% Ejercicio 3. Apartado A

destino0 = cinematica_inv_munneca([10 10 10], 0);
waitforbuttonpress
cla

destino1 = cinematica_inv_munneca([10 10 10], pi/2);
waitforbuttonpress
cla

destino2 = cinematica_inv_munneca([10 10 10], -pi/2);
waitforbuttonpress
cla

%% Ejercicio 3. Apartado B

articulaciones = cinematica_inv_brazo(destino0);
angulo = [articulaciones 0 0];
dibujo_brazo_3d(angulo, pos_ini, pos_fin);
waitforbuttonpress
cla

articulaciones = cinematica_inv_brazo(destino1);
angulo = [articulaciones 0 0];
dibujo_brazo_3d(angulo, pos_ini, pos_fin);
waitforbuttonpress
cla

articulaciones = cinematica_inv_brazo(destino2);
angulo = [articulaciones 0 0];
dibujo_brazo_3d(angulo, pos_ini, pos_fin);
waitforbuttonpress
cla

%% Ejercicio 4. Apartado A

articulaciones = cinematica_inv_brazo(destino0);
rota_m = rotacion_munneca(0, articulaciones, [10 10 10], destino0);
angulo = [articulaciones rota_m 0];
dibujo_brazo_3d(angulo, pos_ini, pos_fin);
waitforbuttonpress
cla

articulaciones = cinematica_inv_brazo(destino0);
rota_m = rotacion_munneca(0, articulaciones, [10 10 10], destino0);
angulo = [articulaciones rota_m 0];
dibujo_brazo_3d(angulo, pos_ini, pos_fin);
waitforbuttonpress
cla

articulaciones = cinematica_inv_brazo(destino0);
rota_m = rotacion_munneca(0, articulaciones, [10 10 10], destino0);
angulo = [articulaciones rota_m 0];
dibujo_brazo_3d(angulo, pos_ini, pos_fin);
waitforbuttonpress
cla

%% Ejercicio 5

ejercicio5([10 10 10 pi/2], [-10 -10 -10 pi/2])
