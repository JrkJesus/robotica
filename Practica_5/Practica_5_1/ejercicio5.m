function ejercicio5 (configuracion1, configuracion2)

punto = configuracion1(1:3);
theta = configuracion1(4);
pos_ini = [0 0 0];
pos_fin = [0 0 0];

destino = cinematica_inv_munneca(punto, theta);
articulaciones = cinematica_inv_brazo(destino);
rota_m = rotacion_munneca(theta, articulaciones, punto, destino);
angulo = [articulaciones rota_m 0];
dibujo_brazo_3d(angulo, pos_ini, pos_fin);

punto = configuracion2(1:3);
theta = configuracion2(4);

destino = cinematica_inv_munneca(punto, theta);
articulaciones = cinematica_inv_brazo(destino);
rota_m = rotacion_munneca(theta, articulaciones, punto, destino);
angulo = [articulaciones rota_m 0];
dibujo_brazo_3d(angulo, pos_ini, pos_fin);