function solucion=cinematica_inv_brazo(punto)

l1=5.5;
l2=12;
l3=12;

x0=punto(1);
y0=punto(2);
z0=punto(3);

% 
% 
% 
% z0=23;
% x0=-1;
% y0=-1;

q1=atan2(y0,x0);

%coordenada x en el plano
x2=sqrt(x0^2+y0^2);

if z0>=l1
   
	q3=acos((x2^2+(z0-l1)^2-l2^2-l3^2)/(2*l2*l3));

    q2=atan2(x2,(z0-l1))-atan2(l3*sin(q3),(l2+l3*cos(q3)));
    

 else
    
    q3=-(acos((x2^2+(l1-z0)^2-l2^2-l3^2)/(2*l2*l3)));
 
 	q2=(atan2(-x2,(l1-z0))-atan2(l3*sin(q3),(l2+l3*cos(q3))))+pi;
 
end

solucion=[q1 q2 q3];
   
%Puntos situados en las articulaciones

p0=[0,0,0];
p1=[0,0,l1];

A=l2*cos((pi/2)-q2);
B=l1+l2*sin((pi/2)-q2);

p2=[A*cos(q1),A*sin(q1),B];

p3=[(A+l3*cos((pi/2)-(q2+q3)))*cos(q1),(A+l3*cos((pi/2)-(q2+q3)))*sin(q1), B+l3*sin((pi/2)-(q2+q3))];

%Matriz de puntos de las articulaciones
puntos=[p0;p1;p2;p3];

%dibuja los enlaces
plot3(puntos(:,1),puntos(:,2),puntos(:,3));

hold on

%%Dibuja los puntos de las articulaciones
plot3(p0(1),p0(2),p0(3),'*r')
plot3(p1(1),p1(2),p1(3),'*r')
plot3(p2(1),p2(2),p2(3),'*r')
plot3(p3(1),p3(2),p3(3),'or')
plot3(x0,y0,z0,'*k')
grid on

axis([-5 12 -5 12])