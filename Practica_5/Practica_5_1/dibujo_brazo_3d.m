function dibujo_brazo_3d(angulo,pos_ini,pos_fin)

q1=angulo(1);
q2=angulo(2);
q3=angulo(3);
q4=angulo(4);
q5=angulo(5);

% Primero definimos el tama�o del manipulador
ab=5; % ancho de la base
lb=5; % largo de la base
l1=5.5; % alto del enlace

ab1=2; % ancho del brazo 1
lb1=2; % largo del brazo 1
l2=12; % longitud del enlace

ab2=2; % ancho del brazo 2
lb2=2; % largo del brazo 2
l3=12; % longitud del enlace

ab3=2; % ancho del brazo 2
lb3=2; % largo del brazo 2
l4=6; % longitud del enlace

am=2; % ancho de la mano
lm=2; % largo de la mano
l5=6; % longitud de la mano

% Creamos los enlaces de forma local

% Enlace 0

p11=[ab/2 lb/2 0 1]';
p21=[ab/2 lb/2 l1 1]';
p31=[ab/2 -lb/2 0 1]';
p41=[ab/2 -lb/2 l1 1]';
p51=[-ab/2 lb/2 0 1]';
p61=[-ab/2 lb/2 l1 1]';
p71=[-ab/2 -lb/2 0 1]';
p81=[-ab/2 -lb/2 l1 1]';

enlace_0= [ p11 p21 p41 p31 p11 p51 p61 p21 p11 p51 p71 p31 p41 p81 p61 p51 p71 p81] ;

% Enlace 1

p12=[ab1/2 lb1/2 0 1]';
p22=[ab1/2 lb1/2 l2 1]';
p32=[ab1/2 -lb1/2 0 1]';
p42=[ab1/2 -lb1/2 l2 1]';
p52=[-ab1/2 lb1/2 0 1]';
p62=[-ab1/2 lb1/2 l2 1]';
p72=[-ab1/2 -lb1/2 0 1]';
p82=[-ab1/2 -lb1/2 l2 1]';

enlace_1= [ p12 p22 p42 p32 p12 p52 p62 p22 p12 p52 p72 p32 p42 p82 p62 p52 p72 p82] ;

% Enlace 2

p13=[ab2/2 lb2/2 0 1]';
p23=[ab2/2 lb2/2 l3 1]';
p33=[ab2/2 -lb2/2 0 1]';
p43=[ab2/2 -lb2/2 l3 1]';
p53=[-ab2/2 lb2/2 0 1]';
p63=[-ab2/2 lb2/2 l3 1]';
p73=[-ab2/2 -lb2/2 0 1]';
p83=[-ab2/2 -lb2/2 l3 1]';

enlace_2= [ p13 p23 p43 p33 p13 p53 p63 p23 p13 p53 p73 p33 p43 p83 p63 p53 p73 p83] ;

% Enlace 3

p14=[ab3/2 lb3/2 0 1]';
p24=[ab3/2 lb3/2 l4 1]';
p34=[ab3/2 -lb3/2 0 1]';
p44=[ab3/2 -lb3/2 l4 1]';
p54=[-ab3/2 lb3/2 0 1]';
p64=[-ab3/2 lb3/2 l4 1]';
p74=[-ab3/2 -lb3/2 0 1]';
p84=[-ab3/2 -lb3/2 l4 1]';

enlace_3= [ p14 p24 p44 p34 p14 p54 p64 p24 p14 p54 p74 p34 p44 p84 p64 p54 p74 p84] ;

% Mano

p15=[0 0 0 1]';
p25=[0 0 l5/3 1]';
p35=[am/2 0 l5/3 1]';
p45=[-am/2 0 l5/3 1]';
p55=[am/2 -lm/2 2*l5/3 1]';
p65=[-am/2 -lm/2 2*l5/3 1]';
p75=[am/2 -lm/2 l5 1]';
p85=[-am/2 -lm/2 l5 1]';
p95=[am/2 lm/2 2*l5/3 1]';
p105=[-am/2 lm/2 2*l5/3 1]';
p115=[am/2 lm/2 l5 1]';
p125=[-am/2 lm/2 l5 1]';

mano=[p15 p25 p35 p45 p65 p85 p75 p55 p35 p95 p115 p125 p105 p45 p35 p95 p105 p45 p65 p55];

% Ahora comenzamos con las transformaciones

% Enlace 0

Rot_z=Rotacionz(q1);
T_enlace_0=Rot_z;
enlace_0_p=T_enlace_0*enlace_0;

% Enlace 1

Rot_z=Rotacionz(q1);
Tras_z=desplazamiento(0,0,l1);
Rot_y=Rotaciony(q2);
T_enlace_1=Tras_z*Rot_z*Rot_y;
enlace_1_p=T_enlace_1*enlace_1;

% Enlace 2

A=l2*cos((pi/2)-q2);
B=l1+l2*sin((pi/2)-q2);
% p_salida2 es el punto de partida del enlace 2
p_salida2=[A*cos(q1),A*sin(q1),B];
% Buscamos la matriz que nos traslada el origen a dicho punto
Tras = desplazamiento(p_salida2(1), p_salida2(2),p_salida2(3));
Rot_z=Rotacionz(q1);
Rot_y=Rotaciony(q2+q3);
T_enlace_2=Tras*Rot_z*Rot_y;
enlace_2_p=T_enlace_2*enlace_2;

% Enlace 3

Ap=A+l3*cos((pi/2)-(q2+q3));
Bp=B+l3*sin((pi/2)-(q2+q3));
p_salida3=[Ap*cos(q1),Ap*sin(q1), Bp];
Tras = desplazamiento(p_salida3(1), p_salida3(2),p_salida3(3));
Rot_z=Rotacionz(q1);
Rot_y=Rotaciony(q2+q3+q4);
T_enlace_3=Tras*Rot_z*Rot_y;
enlace_3_p=T_enlace_3*enlace_3;

% Mano

App=Ap+l4*cos((pi/2)-(q2+q3+q4));
Bpp=Bp+l4*sin((pi/2)-(q2+q3+q4));
p_salida4=[App*cos(q1),App*sin(q1), Bpp];
Tras = desplazamiento(p_salida4(1),p_salida4(2),p_salida4(3));
if (q5==0)
    Rot_z=Rotacionz(q1);
    Rot_y=Rotaciony(q2+q3+q4);
    T_mano=Tras*Rot_z*Rot_y;
else
    Rot_z=Rotacionz(q1);
    Rot_y=Rotaciony(q2+q3+q4);
    Rot_x=Rotacionx(q5);
    Rot_p=Rot_z*Rot_x*Rot_y;
    T_mano=Tras*Rot_p;
end

mano_p=T_mano*mano;

% Representamos el manipulador

%close all

plot3(pos_ini(1),pos_ini(2),pos_ini(3),'*g');
hold on
plot3(pos_fin(1),pos_fin(2),pos_fin(3),'*k');
plot3(enlace_0_p(1,:),enlace_0_p(2,:),enlace_0_p(3,:),'r');
plot3(enlace_1_p(1,:),enlace_1_p(2,:),enlace_1_p(3,:));
plot3(enlace_2_p(1,:),enlace_2_p(2,:),enlace_2_p(3,:));
plot3(enlace_3_p(1,:),enlace_3_p(2,:),enlace_3_p(3,:));
plot3(mano_p(1,:),mano_p(2,:),mano_p(3,:));

axis([-30 30 -35 35 -35 35]);

xlabel('Eje x');
ylabel('Eje y');
zlabel('Eje z');
grid on
